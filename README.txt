Names: Charles Fitzhenry, Darren Silke, Matthew Williams
Student Numbers: FTZCHA002, SLKDAR001, WLLMAT011
Date: 13 October 2015

Game Name: Equilibrium

Description:

The game is a two-player, sci-fi, 3D, first-person shooter with physics based gameplay.

Instructions on how to play the game are given during gameplay. Instructions on how to control the players can be accessed by clicking on the 'ABOUT' button in the main menu.

Instructions:

1. Run the game using the packaged .exe file.
2. Follow the on-screen instructions in terms of clicking on the appropriate buttons.
3. To exit the game, click on the 'QUIT GAME' button in the main menu. During gameplay, press the escape key to return to the main menu.