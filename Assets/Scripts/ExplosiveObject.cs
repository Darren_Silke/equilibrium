﻿using UnityEngine;
using System.Collections;

public class ExplosiveObject : MonoBehaviour
{
    public ParticleEmitter explosionEffect;             // A particle emitter for the explosion effect when an explosive object is destroyed.
    public AudioClip explosionClip;                     // An audio clip to play when the explosion effect happens.
    public bool destroyObject = false;                  // A flag to indicate whether the object should be destroyed on collision with another object.
    
    public float explosionRadius = 1.0f;               // The radius of the sphere within which the explosion has its effect. 
    private float explosionForce = 2000.0f;             // The force of the explosion (which may be modified by distance).
    
	public void DestroyObject()
    {
        Vector3 explosionPosition = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, 1.0f);
            }
            
            // Kill enemy
            if (hit.gameObject.tag == Tags.enemy)
            {
                // The enemy takes 50% damage.
                hit.gameObject.GetComponent<EnemyHealth>().DecrementHealth(50);
            }
            
            if(hit.gameObject.tag == Tags.player)
            {
                // The player takes 25% damage.
                hit.gameObject.GetComponent<PlayerHealth>().DecrementHealth(25);
            }
        }
        
        Instantiate(explosionEffect, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(explosionClip, transform.position); 
		Debug.Log ("Destroyed");
        Destroy(gameObject);
    }
    
    void OnCollisionEnter(Collision collision)
    {
        if (destroyObject)
        {
            DestroyObject();
        }
    }
}
