﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
    // A list of tag strings.
    public const string player = "Player";
    public const string pickUpObject = "PickUpObject";
    public const string enemy = "Enemy";
    public const string checkpoint = "Checkpoint";
    public const string puzzleHint = "PuzzleHint";
    public const string muzzleFlash = "MuzzleFlash";
    public const string weapon = "Weapon";
    public const string weaponPickUp = "WeaponPickUp";
    public const string gameController = "GameController";
    public const string mainCamera = "MainCamera";
    public const string explosiveObject = "ExplosiveObject";
    public const string fader = "Fader";
    public const string endScene = "EndScene";
	public const string toggleSwitch = "ToggleSwitch";
	public const string weaponHalo = "WeaponHalo";

	//Animations
}
