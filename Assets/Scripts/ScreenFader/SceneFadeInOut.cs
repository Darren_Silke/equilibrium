﻿using UnityEngine;
using System.Collections;

public class SceneFadeInOut : MonoBehaviour
{
    public float fadeSpeed = 0.3f;           // Speed that the screen fades to and from black.
    public int levelToLoad;
    private bool sceneStarting = true;      // Whether or not the scene is still fading in.
    private RespawnPlayer respawnPlayer;    // Reference to the RespawnPlayer script.
    private PlayerHealth playerHealth;
    private FirstPersonController firstPersonController;

    void Awake()
    {
        // Setting up the references.
        respawnPlayer = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<RespawnPlayer>();
        playerHealth = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<PlayerHealth>();
        firstPersonController = GameObject.FindGameObjectWithTag(Tags.player).GetComponent<FirstPersonController>();
        // Set the texture so that it is the size of the screen and covers it.
        guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
    }

    void Update()
    {
        // If the scene is starting...
        if (sceneStarting)
            // ... call the StartScene function.
            StartScene();
    }

    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        guiTexture.color = Color.Lerp(guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
    }

    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.
        guiTexture.color = Color.Lerp(guiTexture.color, Color.black, fadeSpeed * Time.deltaTime);
    }

    void StartScene()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if (guiTexture.color.a <= 0.025f)
        {
            // ... set the colour to clear and disable the GUITexture.
            guiTexture.color = Color.clear;
            guiTexture.enabled = false;

            // The scene is no longer starting.
            sceneStarting = false;
        }
    }

    public void EndScene()
    {
        // Make sure the texture is enabled.
        guiTexture.enabled = true;

        // Start fading towards black.
        FadeToBlack();
        // If the screen is almost black...
        if (guiTexture.color.a >= 0.5f)
        {
            // ... proceed to next level.
            Application.LoadLevel(levelToLoad);
           
        }
    }

    public void SpawnPlayer()
    {
        // Make sure the texture is enabled.
        guiTexture.enabled = true;

        // Start fading towards black.
        FadeToBlack();
        // If the screen is almost black...
        if (guiTexture.color.a >= 0.5f)
        {
            // ... respawn the player.
            respawnPlayer.SpawnPlayer();
            firstPersonController.enabled = true;
            playerHealth.playerDead = false;
            playerHealth.Health = 100;
            sceneStarting = true;
        }
    }
}
