﻿using UnityEngine;
using System.Collections;

public class PlayerToggleSwitch : MonoBehaviour {

	private GameObject cameraObj;
	private GameObject hitObject;
	public float maxDist = 2.0f;

	void Start(){

		cameraObj = transform.FindChild("Camera").gameObject;
	}

	public void toggleSwitch(bool pickUpButton){
		
		RaycastHit hit;
		Ray ray = new Ray(cameraObj.transform.position, cameraObj.transform.forward);
		
		if(pickUpButton){
			
			if (Physics.Raycast(ray, out hit, maxDist))
			{
				hit.transform.SendMessage("HitByRaycast", gameObject, SendMessageOptions.DontRequireReceiver);
			}
			
		}
		
	}
}
