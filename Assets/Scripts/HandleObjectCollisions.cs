﻿using UnityEngine;
using System.Collections;

/*
 *  Attach this script to a dynamic object to do something upon object collisions
 *  e.g play a sound when the object collides with something
 * 
 */

public class HandleObjectCollisions : MonoBehaviour
{

    private AudioSource audioSource;
    public AudioClip collisionSound;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {

        audioSource.clip = collisionSound;
        audioSource.Play();

    }
}
