﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
	public GameObject bloodSpatterEffect;               // A game object containing a particle system to simulate blood spatter.
	//public AudioClip deathClip;                         // A sound effect for when the enemy dies.
	
	private int health = 100;                           // The enemy's initial health.
	private Animator anim;                              // Reference to the animator component.
	private HashIDs hash;                               // Reference to the HashIDs script.
	private NavMeshAgent navMeshAgent;                  // Reference to the NavMeshAgent component.
	private EnemySight enemySight;						// Reference to the EnemySight script.
	private EnemyAnimation enemyAnimation;              // Reference to the EnemyAnimation script.
	private EnemyShooting enemyShooting;                // Reference to the EnemyShooting script.
	private EnemyAI enemyAI;                            // Reference to the EnemyAI script.
	
	void Awake()
	{
		// Setting up the references.
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
		navMeshAgent = GetComponent<NavMeshAgent>();
		enemySight = GetComponent<EnemySight>();
		enemyAnimation = GetComponent<EnemyAnimation>();
		enemyShooting = GetComponent<EnemyShooting>();
		enemyAI = GetComponent<EnemyAI>();
	}
	
	public void DecrementHealth(int value)
	{
		if(health > 0)
		{
			Instantiate(bloodSpatterEffect, transform.position + new Vector3(0.0f, 2.0f, 0.0f), Quaternion.identity);
		}	
		
		health -= value;
		
		if(health <= 0)
		{
			//AudioSource.PlayClipAtPoint(deathClip, transform.position);
			navMeshAgent.enabled = false;
			enemySight.enabled = false;
			enemyAnimation.enabled = false;
			enemyShooting.enabled = false;
			enemyAI.enabled = false;
			anim.SetBool(hash.playerInSightBool, false);
			anim.SetBool(hash.deadBool, true);
			Destroy(gameObject, 5.0f);
		}
	}
}
