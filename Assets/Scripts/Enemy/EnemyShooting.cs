﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour
{
    public float maximumDamage = 100f;                  // The maximum potential damage per shot.
    public float minimumDamage = 0f;                    // The minimum potential damage per shot.
    public AudioClip shotClip;                          // An audio clip to play when a shot happens.
    public float flashIntensity = 3f;                   // The intensity of the light when the shot happens.
    public float fadeSpeed = 10f;                       // How fast the light will fade after the shot.

    private Animator anim;                              // Reference to the animator.
    private HashIDs hash;                               // Reference to the HashIDs script.
    private LineRenderer laserShotLine;                 // Reference to the laser shot line renderer.
    private Light laserShotLight;                       // Reference to the laser shot light.
    private SphereCollider col;                         // Reference to the sphere collider.
    private Transform player;                           // Reference to the player's transform.
    private PlayerHealth playerHealth;                  // Reference to the player's health.
    private bool shooting;                              // A bool to say whether or not the enemy is currently shooting.
    private float scaledDamage;                         // Amount of damage that is scaled by the distance from the player.
	
	private GameObject[] players;
	private float distP1;
	private float distP2;

    void Awake()
    {
        // Setting up the references.
        anim = GetComponent<Animator>();
        laserShotLine = GetComponentInChildren<LineRenderer>();
        laserShotLight = laserShotLine.gameObject.light;
        col = GetComponent<SphereCollider>();

		players = GameObject.FindGameObjectsWithTag(Tags.player);
		FindClosestPlayer();

        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();

        // The line renderer and light are off to start.
        laserShotLine.enabled = false;
        laserShotLight.intensity = 0f;

        // The scaledDamage is the difference between the maximum and the minimum damage.
        scaledDamage = maximumDamage - minimumDamage;
    }

	void FindClosestPlayer(){
		
		/*Find closest player*/
		GameObject p1 = players[0];
		GameObject p2 = players[1];
		
		distP1 = Vector3.Distance(p1.transform.position, transform.position);
		distP2 = Vector3.Distance(p2.transform.position, transform.position);
		
		if(distP1 <= distP2){
			player = p1.transform;
			Debug.Log("Current target: P1");
		}
		else{
			player = p2.transform;
			Debug.Log("Current target:");
		}

		playerHealth = player.gameObject.GetComponent<PlayerHealth>();
	}	

    void Update()
    {
		FindClosestPlayer();


        // Cache the current value of the shot curve.
        float shot = anim.GetFloat(hash.shotFloat);

        // If the shot curve is peaking and the enemy is not currently shooting...
        if (shot > 0.5f && !shooting)
            // ... shoot.
            Shoot();

        // If the shot curve is no longer peaking...
        if (shot < 0.5f)
        {
            // ... the enemy is no longer shooting and disable the line renderer.
            shooting = false;
            laserShotLine.enabled = false;
        }

        // Fade the light out.
        laserShotLight.intensity = Mathf.Lerp(laserShotLight.intensity, 0f, fadeSpeed * Time.deltaTime);
    }

    void OnAnimatorIK(int layerIndex)
    {
        // Cache the current value of the AimWeight curve.
        float aimWeight = anim.GetFloat(hash.aimWeightFloat);

        // Set the IK position of the right hand to the player's centre.
        anim.SetIKPosition(AvatarIKGoal.RightHand, player.position + new Vector3(0, 0.8f, 0));

        // Set the weight of the IK compared to animation to that of the curve.
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, aimWeight);
    }

    void Shoot()
    {
        // The enemy is shooting.
        shooting = true;

        // The fractional distance from the player, 1 is next to the player, 0 is the player is at the extent of the sphere collider.
        float fractionalDistance = (col.radius - Vector3.Distance(transform.position, player.position)) / col.radius;

        // The damage is the scaled damage, scaled by the fractional distance, plus the minimum damage.
        float damage = scaledDamage * fractionalDistance + minimumDamage;

        // The player takes damage.

        playerHealth.DecrementHealth((int)damage);

        // Display the shot effects.
        ShotEffects();
    }

    void ShotEffects()
    {
        // Set the initial position of the line renderer to the position of the muzzle.
        laserShotLine.SetPosition(0, laserShotLine.transform.position);

        // Set the end position to the player's centre of mass.
        laserShotLine.SetPosition(1, player.position + new Vector3(0, 0.8f, 0));

        // Turn on the line renderer.
        laserShotLine.enabled = true;

        // Make the light flash.
        laserShotLight.intensity = flashIntensity;

        // Play the gun shot clip at the position of the muzzle flare.
        AudioSource.PlayClipAtPoint(shotClip, laserShotLight.transform.position);
    }
}
