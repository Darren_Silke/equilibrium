﻿using UnityEngine;
using System.Collections;

/*
 *  Script to respawn the Player at the last checkpoint if the player
 *  falls below a certain height.
 * 
 */

public class RespawnPlayer : MonoBehaviour
{

    public int respawnHeight = 0;          // Height at which respawn should be initialized.

    private Transform lastCheckpoint;

    void Update()
    {
        if (transform.position.y <= respawnHeight)
            SpawnPlayer();
    }

    void OnTriggerEnter(Collider other)
    {
        // Determine last checkpoint triggered.
        if (other.tag == Tags.checkpoint) 
            lastCheckpoint = other.gameObject.transform;
    }

    public void SpawnPlayer()
    {
        transform.position = new Vector3(lastCheckpoint.position.x, lastCheckpoint.position.y + 2, lastCheckpoint.position.z);
        transform.rotation = lastCheckpoint.rotation;
    }
}
