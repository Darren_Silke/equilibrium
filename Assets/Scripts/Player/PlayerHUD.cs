﻿using UnityEngine;
using System.Collections;

/*
 * Simple script to display information for the HUD, such as the player health, 
 * current weapon and ammo count
 */ 

public class PlayerHUD : MonoBehaviour
{
    // gui
    public Font BigFont;
    public Font SmallFont;
    public Texture Background = null;
    public float BigFontOffset = 69;
    public float SmallFontOffset = 56;
    protected float m_CurrentHealthOffset = 0;
    protected float m_CurrentAmmoOffset = 200;
    protected Vector2 m_DrawPos = Vector2.zero;
    protected Vector2 m_DrawSize = Vector2.zero;
    protected Rect m_DrawLabelRect = new Rect(0, 0, 0, 0);
    protected Rect m_DrawShadowRect = new Rect(0, 0, 0, 0);

    //Crosshair
    public Texture imageCrosshair;
    public Texture imageCrosshair2;

    //Health
    public Texture2D HealthIcon = null;
    public Color HealthColor = Color.white;

    protected float m_HealthWidth { get { return ((HealthStyle.CalcSize(new GUIContent(healthValue)).x)); } }

    //Ammo
    public Texture2D AmmoIcon = null;
    public string currentAmmoAmount = "10";
    public string currentWeaponClipCount = "3";
    public Color AmmoColor = Color.white;
    private Weapon weapon;

    //Telekinisis
    public Texture2D TelekinesisIcon = null;
    private PlayerHealth playerHealth;
    private string healthValue; //Replace with call to HealthScript

    //Weapon Icon
    public Texture2D WeaponIcon = null;

    //colors
    protected Color m_TranspBlack = new Color(0, 0, 0, 0.5f);

    //player mode
    private PlayerInteractionMode interactionMode;

    //Event handler
    private HandleEvent eventHandler;

	private Camera camera;

	private float crosshairX = Screen.width * 0.25f;
	private float crosshairY = Screen.height * 0.5f;
	
	private float screenWidth = Screen.width * 0.5f;
	private float screenHeight = Screen.height;

    void Start()
    {
        playerHealth = GetComponent<PlayerHealth>();
        healthValue = playerHealth.Health.ToString();
        interactionMode = GetComponent<PlayerInteractionMode>();
		weapon = transform.FindChild("Camera/Weapon").GetComponent<Weapon>();
        eventHandler = GetComponent<HandleEvent>();
		camera = transform.FindChild("Camera").GetComponent<Camera>();
    }

    void OnGUI()
    {
        DisplayCrosshair();

        DrawHealth();

        DrawAmmo();

        if (eventHandler.hintIsTriggered())
        {

            DrawHintMessage();
            
        }
    }

    private void DisplayCrosshair()
    {

        //Use different crosshairs for telekinesis and shooting
        if (interactionMode.getMode() == PlayerInteractionMode.InteractionMode.Shooting)
        {
			GUI.DrawTexture(new Rect((crosshairX) - (imageCrosshair.width * 0.5f),
			                         (crosshairY) - (imageCrosshair.height * 0.5f), imageCrosshair.width,
                                       imageCrosshair.height), imageCrosshair);
            GUI.color = Color.white;
        }
        else{

			GUI.DrawTexture(new Rect((crosshairX) - (imageCrosshair2.width * 0.5f),
			                         (crosshairY) - (imageCrosshair2.height * 0.5f), imageCrosshair2.width,
                                     imageCrosshair2.height), imageCrosshair2);
            GUI.color = Color.white;

        }
    }

    private void DrawHintMessage()
    {

        float offset = 250;
        Vector2 box = new Vector2(250, 52);

		DrawLabel("", new Vector2(screenWidth - offset, screenHeight / 2), box, HintStyle, AmmoColor, m_TranspBlack, null);   // background
		DrawLabel(eventHandler.getInputKey(), new Vector2(screenWidth - offset + 15, screenHeight / 2), box, HintStyle, AmmoColor, Color.clear, null);        // Input Key
		DrawLabel(eventHandler.getMessage(), new Vector2(screenWidth - offset - 15, screenHeight / 2), box, HintStyleSmall, AmmoColor, Color.clear, null);        // Message
    }

    private void DrawHealth()
    {
        if (playerHealth.Health < 0)
        {
            healthValue = "0";
        } else
        {
            healthValue = playerHealth.Health.ToString();
        }

		DrawLabel("", new Vector2(m_CurrentHealthOffset, screenHeight - 68), new Vector2(80 + m_HealthWidth, 52), AmmoStyle, Color.white, m_TranspBlack, null);    // background
        if (HealthIcon != null)
			DrawLabel("", new Vector2(m_CurrentHealthOffset + 10, screenHeight - 58), new Vector2(32, 32), AmmoStyle, Color.white, HealthColor, HealthIcon);           // icon
		DrawLabel(healthValue, new Vector2(m_CurrentHealthOffset - 18 - (45 - m_HealthWidth), screenHeight - BigFontOffset), new Vector2(110, 60), HealthStyle, HealthColor, Color.clear, null);   // value
		DrawLabel("%", new Vector2(m_CurrentHealthOffset + 50 + m_HealthWidth, screenHeight - SmallFontOffset), new Vector2(110, 60), AmmoStyleSmall, HealthColor, Color.clear, null); // percentage mark
        GUI.color = Color.white;    
    }

    private void DrawAmmo()
    {

        DrawLabel("", new Vector2(screenWidth - m_CurrentAmmoOffset, screenHeight - 68), new Vector2(200, 52), HintStyle, AmmoColor, m_TranspBlack, null);    // background

        if (interactionMode.getMode() == PlayerInteractionMode.InteractionMode.Shooting)
        {

            currentAmmoAmount = weapon.shots.ToString();
            currentWeaponClipCount = weapon.rounds.ToString();
            if (AmmoIcon != null)
				DrawLabel("", new Vector2(screenWidth - m_CurrentAmmoOffset + 10, screenHeight - 58), new Vector2(32, 32), AmmoStyle, Color.white, AmmoColor, AmmoIcon);  // icon
			DrawLabel(currentAmmoAmount + "/" + currentWeaponClipCount, new Vector2(screenWidth - m_CurrentAmmoOffset + 15, screenHeight - BigFontOffset), new Vector2(110, 60), AmmoStyle, AmmoColor, Color.clear, null);        // value
            if (WeaponIcon != null)
				DrawLabel("", new Vector2(screenWidth - m_CurrentAmmoOffset + 150, screenHeight - 58), new Vector2(32, 32), AmmoStyle, Color.white, AmmoColor, WeaponIcon);  // icon
        }
        else{

			DrawLabel("Telekinesis", new Vector2(screenWidth - m_CurrentAmmoOffset + 10, screenHeight - BigFontOffset - 2), new Vector2(110, 60), TextStyleSmall, AmmoColor, Color.clear, null);        // value
            if (WeaponIcon != null)
				DrawLabel("", new Vector2(screenWidth - m_CurrentAmmoOffset + 150, screenHeight - 58), new Vector2(32, 32), AmmoStyle, Color.white, AmmoColor, TelekinesisIcon);  // icon

        }

    }
    
    private void DrawLabel(string text, Vector2 position, Vector2 scale, GUIStyle textStyle, Color textColor, Color bgColor, Texture texture)
    {

        if (texture == null)
            texture = Background;

        if (scale.x == 0)
            scale.x = textStyle.CalcSize(new GUIContent(text)).x;
        if (scale.y == 0)
            scale.y = textStyle.CalcSize(new GUIContent(text)).y;
        
        m_DrawLabelRect.x = m_DrawPos.x = position.x;
        m_DrawLabelRect.y = m_DrawPos.y = position.y;
        m_DrawLabelRect.width = m_DrawSize.x = scale.x;
        m_DrawLabelRect.height = m_DrawSize.y = scale.y;
        
        if (bgColor != Color.clear)
        {
            GUI.color = bgColor;
            if (texture != null)
                GUI.DrawTexture(m_DrawLabelRect, texture);
        }
        
        GUI.color = textColor;
        GUI.Label(m_DrawLabelRect, text, textStyle);
        GUI.color = Color.white;
        
        m_DrawPos.x += m_DrawSize.x;
        m_DrawPos.y += m_DrawSize.y;
    }

    protected GUIStyle m_HealthStyle = null;

    public GUIStyle HealthStyle
    {
        get
        {
            if (m_HealthStyle == null)
            {
                m_HealthStyle = new GUIStyle("Label");
                m_HealthStyle.font = BigFont;
                m_HealthStyle.alignment = TextAnchor.MiddleRight;
                m_HealthStyle.fontSize = 28;
                m_HealthStyle.wordWrap = false;
            }
            return m_HealthStyle;
        }
    }
    
    protected GUIStyle m_AmmoStyle = null;

    public GUIStyle AmmoStyle
    {
        get
        {
            if (m_AmmoStyle == null)
            {
                m_AmmoStyle = new GUIStyle("Label");
                m_AmmoStyle.font = BigFont;
                m_AmmoStyle.alignment = TextAnchor.MiddleRight;
                m_AmmoStyle.fontSize = 28;
                m_AmmoStyle.wordWrap = false;
            }
            return m_AmmoStyle;
        }
    }
    
    protected GUIStyle m_AmmoStyleSmall = null;

    public GUIStyle AmmoStyleSmall
    {
        get
        {
            if (m_AmmoStyleSmall == null)
            {
                m_AmmoStyleSmall = new GUIStyle("Label");
                m_AmmoStyleSmall.font = SmallFont;
                m_AmmoStyleSmall.alignment = TextAnchor.UpperLeft;
                m_AmmoStyleSmall.fontSize = 15;
                m_AmmoStyleSmall.wordWrap = false;
            }
            return m_AmmoStyleSmall;
        }
    }

    protected GUIStyle m_TextStyleSmall = null;
    
    public GUIStyle TextStyleSmall
    {
        get
        {
            if (m_TextStyleSmall == null)
            {
                m_TextStyleSmall = new GUIStyle("Label");
                m_TextStyleSmall.font = SmallFont;
                m_TextStyleSmall.alignment = TextAnchor.MiddleRight;
                m_TextStyleSmall.fontSize = 20;
                m_TextStyleSmall.wordWrap = false;
            }
            return m_TextStyleSmall;
        }
    }

    protected GUIStyle m_HintStyle = null;

    public GUIStyle HintStyle
    {
        get
        {
            if (m_HintStyle == null)
            {
                m_HintStyle = new GUIStyle("Label");
                m_HintStyle.font = BigFont;
                m_HintStyle.alignment = TextAnchor.MiddleLeft;
                m_HintStyle.fontSize = 22;
                m_HintStyle.wordWrap = false;
            }
            return m_HintStyle;
        }
    }

    protected GUIStyle m_HintStyleSmall = null;

    public GUIStyle HintStyleSmall
    {
        get
        {
            if (m_HintStyleSmall == null)
            {
                m_HintStyleSmall = new GUIStyle("Label");
                m_HintStyleSmall.font = SmallFont;
                m_HintStyleSmall.alignment = TextAnchor.MiddleRight;
                m_HintStyleSmall.fontSize = 15;
                m_HintStyleSmall.wordWrap = false;
            }
            return m_HintStyleSmall;
        }
    }
}
