﻿using UnityEngine;
using System.Collections;

/*
 * Script attached to player to handle events 
 * such as triggering of a puzzle/tutorial hint or picking up a weapon
 */ 

public class HandleEvent : MonoBehaviour
{
    private bool endScene = false;

    //Tutorial message: Key, Hint
    private string inputKey;
    private string message;
    private bool isTriggered;
    private float timeLeft;

    //Scene fade 
    private SceneFadeInOut sceneFadeInOut;
    private FirstPersonController firstPersonController;

    void Start()
    {
        sceneFadeInOut = GameObject.FindGameObjectWithTag(Tags.fader).GetComponent<SceneFadeInOut>();
        firstPersonController = GetComponent<FirstPersonController>();
    }

    void Update()
    {
        if (endScene)
            sceneFadeInOut.EndScene();
                
        //Update time hint message is displayed
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            isTriggered = false;
        }
    }

    public bool hintIsTriggered()
    {
        return isTriggered;
    }

    public string getInputKey()
    {
        return inputKey;
    }

    public string getMessage()
    {
        return message;
    }

    void OnTriggerEnter(Collider other)
    {
        //Determine whether object collided with is the PuzzleHint
        if (other.gameObject.tag == Tags.puzzleHint)
        {
            isTriggered = true;

            PuzzleHint currentPuzzleHint = other.gameObject.GetComponent<PuzzleHint>(); //Get Puzzle Hint info from triggered puzzle hint

            inputKey = currentPuzzleHint.getInputKey();    //Get puzzle hint message
            message = currentPuzzleHint.getPuzzleHint();    //Get puzzle hint message
            timeLeft = currentPuzzleHint.getDisplayTime();  //Reinitialise timer with puzzle hint display time
        
        }
                
        //Determine if event is end scene
        if (other.gameObject.tag == Tags.endScene)
        {
            endScene = true;
            firstPersonController.enabled = false;
        }
	
    }

    void OnTriggerExit(Collider other)
    {

        if (other.gameObject.tag == Tags.player)
        {
            Debug.Log("On trigger exit");
        }

    }

}
