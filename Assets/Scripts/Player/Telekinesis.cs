﻿using UnityEngine;
using System.Collections;

public class Telekinesis : MonoBehaviour
{
    public float mCorrectionForce = 1000.0f;
    public float throwForce = 500.0f;
    public float pickUpDist = 3.0f;
    public float maxPullDist = 20.0f;
    public float pullFactor = 5.0f;
    public float coolDownTime = 0.3f;
    public AudioClip grabSound;
    public AudioClip throwSound;
    private Vector3 targetPoint;
    private GameObject hitObject;
    private bool held = false;
    private float objectDist;
    private float coolDown = 0;
    private GameObject cameraObj;
	
	private GameObject weaponHalo;
	private float time = 0;

    void Start()
    {
        Screen.showCursor = false;  //Hide cursor upon play
        Screen.lockCursor = true;   //Lock cursor upon play
		cameraObj = transform.parent.gameObject;
		weaponHalo = transform.FindChild("Halo").gameObject;
    }

    public void telekinesisControler(bool pickUpButton, bool throwButton, bool pickUpButtonDown, bool switchModeButton)
    {
		weaponHalo.light.range = 0;
        RaycastHit hit;
        Ray ray = new Ray(cameraObj.transform.position, cameraObj.transform.forward);
            
        //Allow player to pick up object only if object is not being held currently
        if (pickUpButton && !held && coolDown > coolDownTime)
        {
           
            if (Physics.Raycast(ray, out hit, maxPullDist))
            {
                //Debug.DrawRay(ray.origin, ray.direction * maxPullDist, Color.red, 1);
                //numRayCasts++;
                hitObject = hit.collider.gameObject;
                    
                //Only pull/pick up objects tagged 'PickUpObject' or 'ExplosiveObject'.
                if (hitObject.tag == Tags.pickUpObject || hitObject.tag == Tags.explosiveObject || held)
                {

                    objectDist = hit.distance;
                    
                    //Pull object towards player and pickup object if it within pickup distance
                    if (objectDist <= pickUpDist)
                    {
                        held = true;    //Hold object
                        playGrabSound();
                        resetCooldown();
                    } else
                    {
                        pullObject();   //Pull object
                    }
                }
            }

            //Throw held object
        } else if (throwButton && held)
        {
            held = false;
            unFreezeObjectRotation();
            throwObject();
            //

        }   //Drop Object
        else if (pickUpButtonDown && held && coolDown > coolDownTime)
        {
            dropObject();
            unFreezeObjectRotation();
            resetCooldown();
        }
            
        //PickUp Object
        if (held)
        {
            freezeObjectRotation();
            holdObject();
        }
            
            
        coolDown += Time.deltaTime;

    }
    
    private void resetCooldown()
    {
        coolDown = 0;
    }
    
    private void holdObject()
    {
        //weaponDisable();
        //Foward offset
        targetPoint = cameraObj.transform.position;
        //targetPoint += Camera.main.transform.forward * mPointDistance;
        targetPoint += cameraObj.transform.forward * pickUpDist;
        
        Vector3 force = targetPoint - hitObject.transform.position;
        
        Rigidbody rb = hitObject.GetComponent<Rigidbody>() as Rigidbody;
        
        rb.velocity = force.normalized * rb.velocity.magnitude;
        rb.AddForce(force * mCorrectionForce);
        
        rb.velocity *= Mathf.Min(10.0f, force.magnitude / 2);
        
		//Adjust halo effect
		weaponHalo.light.color = new Color(1, 0.078f, 0f); 
		weaponHalo.light.range = 0.2f * Mathf.Abs(Mathf.Sin(time)) + 0.99f;
		time += Time.deltaTime * 10f;

    }

    public void dropObject(){

        held = false;

    }
    
    private void throwObject()
    {
        Rigidbody rb = hitObject.GetComponent<Rigidbody>() as Rigidbody;
        
        Vector3 throwDir = hitObject.transform.position - cameraObj.transform.position;
        
        rb.AddForce(throwDir * throwForce);
        
        playThrowSound();

        if (hitObject.tag == Tags.explosiveObject)
        {
            hitObject.GetComponent<ExplosiveObject>().destroyObject = true;
        }
    }
    
    void pullObject()
    {
        //weaponDisable();
        Rigidbody rb = hitObject.GetComponent<Rigidbody>() as Rigidbody;
        
        //Make object lift higher, (higher elevation)
        
        Vector3 cameraPosition = new Vector3(cameraObj.transform.position.x, cameraObj.transform.position.y + 1, cameraObj.transform.position.z); 
        Vector3 objectPosition = new Vector3(hitObject.transform.position.x, hitObject.transform.position.y - 1, hitObject.transform.position.z);
        
        targetPoint = cameraPosition - objectPosition;  //Pull object towards player
        
        //The force applied to the object is proportional to the distance between the player and the object
        
        rb.AddForce(targetPoint * (pullFactor / objectDist * objectDist));

		weaponHalo.light.color = new Color(0, 0.99f, 0.95f); 
		weaponHalo.light.range = 0.5f * Mathf.Abs(Mathf.Sin(time)) + 0.8f;
		time += Time.deltaTime * 5f;
    }
    
    private void playGrabSound()
    {
        AudioSource.PlayClipAtPoint(grabSound, transform.position, 10.0f);
    }
    
    private void playThrowSound()
    {
        AudioSource.PlayClipAtPoint(throwSound, transform.position, 10.0f);
    }
    
    private void freezeObjectRotation()
    {
        Rigidbody rb = hitObject.GetComponent<Rigidbody>() as Rigidbody;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }
    
    private void unFreezeObjectRotation()
    {
        Rigidbody rb = hitObject.GetComponent<Rigidbody>() as Rigidbody;
        rb.constraints = RigidbodyConstraints.None;
		time = 0;
		weaponHalo.light.range = 0;
    }


}
