﻿using UnityEngine;
using System.Collections;

/*
 *  Script used to change and determine the player's current interaction state/mode.
 *  The player is either in telekinesis mode or shooting mode
 *  The player switches between these two modes using the 'TelekinesisMode' input key
 */ 

public class PlayerInteractionMode : MonoBehaviour
{

    public enum InteractionMode
    {
        Telekinesis,
        Shooting}
    ;

    public InteractionMode currentMode;

    void Start()
    {
        currentMode = InteractionMode.Telekinesis; 
    }

    public void setMode(InteractionMode mode)
    {
		currentMode = mode;
    }

    public void switchMode()
    {
       
        if (currentMode == InteractionMode.Telekinesis)
        {
            currentMode = InteractionMode.Shooting;
            setMode(currentMode);

        } else
        {
            currentMode = InteractionMode.Telekinesis; 
            setMode(currentMode);
        }
    }

    public InteractionMode getMode()
    {
        return currentMode;
    }
}
