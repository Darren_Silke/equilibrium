﻿using UnityEngine;
using System.Collections;

public class HashIDs : MonoBehaviour
{
    // Here the hash tags for various strings used in the animators are stored.
    public int locomotionState;
    public int speedFloat;
    public int playerInSightBool;
    public int shotFloat;
    public int aimWeightFloat;
    public int angularSpeedFloat;
	public int deadBool;

    void Awake()
    {
        locomotionState = Animator.StringToHash("Base Layer.Locomotion");
        speedFloat = Animator.StringToHash("Speed");
        playerInSightBool = Animator.StringToHash("PlayerInSight");
        shotFloat = Animator.StringToHash("Shot");
        aimWeightFloat = Animator.StringToHash("AimWeight");
        angularSpeedFloat = Animator.StringToHash("AngularSpeed");
		deadBool = Animator.StringToHash("Dead");
    }
}
