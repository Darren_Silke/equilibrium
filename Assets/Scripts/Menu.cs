﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    
    public string levelToLoadWhenClickedPlay = "";
    public string[] AboutTextLines = new string[0];
    public Font menuItemFont;
    public Font menuHeadingFont;
    public Font menuTitleFont;
    public int menuItemFontSize = 15;
    public int menuHeadingFontSize = 15;
    public int menuTitleFontSize = 30;
    public AudioClip clip;
    private AudioSource audioSource;
    private SplineController spline;
    private bool showGUI = true;
    private GUIStyle menuItemButtonStyle;
    private GUIStyle menuGUIStyle;
    private GUIStyle titleGUIStyle;
    private GUIStyle SliderGUIStyle;
    private GUIStyle countDownStyle;
    private string clicked = "", MessageDisplayOnAbout = "About \n ";
    private Rect WindowRect = new Rect(20, Screen.height / 2, 200, 400);
    private Rect TitleRect = new Rect(20, (Screen.height / 2) - 100, Screen.width, 80);
    private float volume = 1.0f;
    private bool loadGame = false;
    private int countDownVal = 3;
    private bool doCountDown = false;
    private float dTime = 0;
    
    private void Start()
    {
        spline = GameObject.FindGameObjectWithTag(Tags.mainCamera).GetComponent<SplineController>();
        for (int x = 0; x < AboutTextLines.Length; x++)
        {
            MessageDisplayOnAbout += AboutTextLines [x] + " \n ";
        }
        MessageDisplayOnAbout += "\nPress Esc To Go Back";

        Screen.showCursor = true;  //Hide cursor upon play
        Screen.lockCursor = false;   //Lock cursor upon play
    }

    private void setButtonStyles()
    {
        //Set up styles
        GUI.backgroundColor = Color.clear;
        
        // Create style for a button
        menuItemButtonStyle = new GUIStyle(GUI.skin.button);
        menuItemButtonStyle.fontSize = menuItemFontSize;
        menuItemButtonStyle.font = menuItemFont;
        
        // Set color for selected and unselected buttons
        menuItemButtonStyle.normal.textColor = Color.white;
        menuItemButtonStyle.hover.textColor = Color.black;
        menuItemButtonStyle.alignment = TextAnchor.MiddleLeft;
    }

    private void setGUIStyles()
    {
        // Create style for a button
        titleGUIStyle = new GUIStyle(GUI.skin.label);
        titleGUIStyle.fontSize = menuTitleFontSize;
        titleGUIStyle.font = menuTitleFont;
        
        // Set color for selected and unselected buttons
        titleGUIStyle.normal.textColor = Color.white;
        titleGUIStyle.alignment = TextAnchor.MiddleLeft;

        menuGUIStyle = new GUIStyle(GUI.skin.box);

        menuGUIStyle.fontSize = menuItemFontSize;
        menuGUIStyle.font = menuItemFont;
        
        // Set color for selected and unselected buttons
        menuGUIStyle.normal.textColor = Color.white;
        menuGUIStyle.alignment = TextAnchor.MiddleLeft;

        SliderGUIStyle = new GUIStyle(GUI.skin.horizontalSlider);

        // Set color for selected and unselected buttons
        SliderGUIStyle.normal.textColor = Color.black;
        SliderGUIStyle.alignment = TextAnchor.MiddleLeft;

        countDownStyle = new GUIStyle(GUI.skin.label);
        countDownStyle.fontSize = 100;
        countDownStyle.font = menuHeadingFont;
        
        // Set color for selected and unselected buttons
        countDownStyle.normal.textColor = Color.white;

        countDownStyle.alignment = TextAnchor.MiddleCenter;

    }

    private void drawTitle()
    {
        GUI.Label(TitleRect, "Equilibrium", titleGUIStyle);
       
    }

    private void doSpline(bool loadB)
    {
        if (loadB)
        {
            spline = GameObject.FindGameObjectWithTag(Tags.mainCamera).GetComponent<SplineController>();
            spline.FollowSpline(); 
            showGUI = false;
        }
       
        loadGame = false;
        if (spline.secondLast())
        {
            doCountDown = true;
        }
        if (spline.stillActive())
        { 


            Application.LoadLevel(1);
        }
    }

    private void countDown(bool b)
    {
        if (b && countDownVal > -1)
        {
            GUI.Label(new Rect((Screen.width / 2) - 50, (Screen.height / 2) - 50, 100, 100), countDownVal.ToString(), countDownStyle);
            if (dTime > 3)
            {
                dTime = 0;
                countDownVal--;
            }

            dTime += Time.deltaTime;
        }
    }
    /// <summary>
    /// //////////////////////////////////////////
    /// </summary>
    private void OnGUI()
    {   
        displayGUI(showGUI);  
        doSpline(loadGame);
        countDown(doCountDown);
    }

    public void displayGUI(bool show)
    {
        if (show)
        {
            setButtonStyles();       
            setGUIStyles();
                
            //Draw title
            drawTitle();
                
            if (clicked == "")
            {
                GUI.Window(0, WindowRect, menuFunc, "");
            } else if (clicked == "options")
            {
                GUI.Window(1, WindowRect, optionsFunc, "");
            } else if (clicked == "about")
            {
                GUI.backgroundColor = Color.black;
                GUI.Box(new Rect(0, 0, Screen.width, Screen.height), MessageDisplayOnAbout);
            } 
        }
    }

    private void optionsFunc(int id)
    {
        // Set color for selected and unselected buttons
        SliderGUIStyle.normal.textColor = Color.black;
        SliderGUIStyle.alignment = TextAnchor.MiddleLeft;
         
        GUILayout.Box("Volume", menuGUIStyle);
        volume = GUILayout.HorizontalSlider(volume, 0.0f, 1.0f);
        AudioListener.volume = volume;
        if (GUILayout.Button("Back", menuItemButtonStyle))
        {
            clicked = "";
        }           
    }
    
    private void menuFunc(int id)
    {
        setButtonStyles();
        //buttons 
        if (GUILayout.Button("Play Game", menuItemButtonStyle))
        {
            loadGame = true;
        }
        if (GUILayout.Button("Options", menuItemButtonStyle))
        {
            clicked = "options";
        }
        if (GUILayout.Button("About", menuItemButtonStyle))
        {
            clicked = "about";
        }
        if (GUILayout.Button("Quit Game", menuItemButtonStyle))
        {
            Application.Quit();
        }
    }
    
    private void Update()
    {
        if (clicked == "about" && Input.GetKey(KeyCode.Escape))
            clicked = "";
    }
}
